//uva 10534
//#include <GOD>
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>
#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<ll,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
typedef long long ll;
using namespace std;
ll in[10001];
ll de[10001];
vector<ll>vec;
vector<ll>inn;
vector<ll>dee;

ll bi(ll l , ll h ,ll k ,vector<ll> vv){
    ll mid=(l+h)/2;
    if(h>l){
        if(vv[mid]>=k ) return bi(l, mid ,k ,vv);
        else return bi(mid+1 , h, k ,vv);
    }
    return mid;
}
int main(){
  //  Test;
    ll n ;
    while(cin>>n){
        Set(in ,0);
        Set(de ,0);
        vec.clear();inn.clear();dee.clear();
        Rep(i ,n){
            ll x; cin>>x;vec.push_back(x);
            if(inn.size()==0) inn.push_back(x) , in[i]=1;
            else if(x> inn[inn.size()-1]) inn.push_back(x) ,in[i]=inn.size();
            else{
                ll xx = bi(0 , inn.size()-1, x,inn);
                inn[xx]=x; in[i]=xx+1;
            }
        }
        for(int i = n-1 ;i>=0;i--){
            if(dee.size()==0) dee.push_back(vec[i]) , de[i]=1;
            else if(vec[i]> dee[dee.size()-1]) dee.push_back(vec[i]) ,de[i]+=dee.size();
            else{
                ll xx = bi(0 , dee.size()-1, vec[i],dee);
                dee[xx]=vec[i]; de[i]=xx+1;
            }
        }
        ll mx=0;
        Rep(i ,n)   mx =max(mx , min(de[i] ,in[i] ));cout<<2*mx -1<<endl;
    }
}