//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/
#include <bits/stdc++.h>


#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<pii , long long >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
//#define 	MAXN 		        100000+99
#define 	EPS 		        1e-15
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;


inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}
 //int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
/* int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; */ // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}

}
vll v;
ll arr[100001];
ll pr[100001];
ll bi(ll l ,ll r ,ll k){
    ll m = (l+r)/2;
    while(l<r){
        if(v[arr[m]]>=v[k]) return bi(l, m , k);
        return bi(m+1, r, k);
    }
    return l;
}
int main(){
   // Test;
    ll n ; cin >> n ;
    GA(n ,v);
    Set(pr, -1);
    ll len = 1;
    Rep(i , n){
        if(v[i] <= v[arr[0]]) arr[0]=i;
        else if(v[arr[len-1]] < v[i]) pr[i]  = arr[len-1] , arr[len++] = i;
        else{
            ll m = bi(0 , len-1 , i);
            pr[i] =pr[arr[m]];
            arr[m]=i;
        }
    }
    cout<<len<<endl;
    for(int i = arr[len-1]; i>=0;  i= pr[i]) cout<<v[i]<<" ";
    cout<<endl;

}
